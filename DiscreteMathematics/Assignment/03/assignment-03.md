# Assignment 03

> Write a code in C programming for obtaining the powerset of a given set

## Parameters

- A series of a character for a set
- The maximum number of elements in a given set is 10

## Return Values

- Format : [####] character, character, character, ...
- The serial number that counts the number of subsets in the 4-digits format
- A list of characters that represent a subset of a given set

## Examples

```console
$ program.exe a b
```

## Print out

- The output of all the subsets of a given set
- Personal signature that can identify the writer in a unique way
- The output of the computation is followed by the personal signature
- Print out the symbol $`\varnothing`$ (ASCII code: 155) for the empty set

```console
$ program.exe a b
[0001] 'symbol for the empty set'
[0002] a
[0003] b
[0004] a, b
*********************
Byung-Woo Hong
Student ID : 12345678
*********************
```

## Submission 

- Source code [.c]
- git commit history [.pdf]
- Screen capture of the result for the print out with an example input [.png]

## File naming convention

- Source code : STUDENTID_ASSIGNMENT##.c [example: 20191234_03.c]
- git commit history : STUDENTID_ASSIGNMENT##.pdf [example: 20191234_03.pdf]
- Screen capture of an example output : STUDENTID_ASSIGNMENT##.png [example: 20191234_03.png]
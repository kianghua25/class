# Assignments for Machine Learning Project

### 1. Binary classification based on logistic regression using point clusters

### 2. Binary classification based on logistic regression using images

### 3. Binary classification based on 3 layers neural network (input, hidden, hidden, output)

### 4. Activation functions (sigmoid, tanh, ReLU, leaky ReLU)

### 5. Customized network architecture with your choice of activation function and the number of hidden units and also initialization

### 6. Regularization (weight decay and Dropout)

# Class lectures for Machine Learning Project

### 1. Binary classification based on logistic regression (video : C1.W1.L01 - C1.W2.L08)

### 2. Optimization of the objective function defined by cross entropy via vectorization (video : C1.W2.L09 - C1.W2.L14)

### 3. Neural Network (video : C1.W2.L15 - C1.C3.L05)

### 4. Activation functions (video : C1.C3.L06 - C1.C3.L08)

### 5. Initialization and optimization (gradient descent) (video : C1.C3.L09 - C1.C3.L11)

### 6. Regularization (weight decay and Dropout) (video : C1 Week4 - C2 Week1)
